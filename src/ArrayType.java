public class ArrayType extends Type {

	private final Type type;
	private final int size;

	public ArrayType(Type type, int size) {
		this.type = type;
		this.size = size;
	}

	public int size() {
		return type.size() * size;
	}

	@Override
	public int getDimension() {
		return type.getDimension() + 1;
	}

	@Override
	public boolean equals(Object other) {
		if(other instanceof ArrayType)
			return true;
		return false;
	}

	public Type getBaseType() {
		if (type.getDimension() == 0)
			return type;
		return type.getBaseType();
	}

	@Override
	public String getIdentifier() {

		return toString() + ":" + type.getBaseType() + ":" + getDimension();
	}

	@Override
	public String toString() {
		return "array";
//		return "array(" + size + ", " + type.toString() + ")";
	}
}