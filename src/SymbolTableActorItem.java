public class SymbolTableActorItem extends SymbolTableItem{
    public SymbolTableActorItem (Actor actor) {
        this.actor = actor;
    }

    @Override
    public String getKey() {
        return this.actor.getName();
    }

    @Override
    public boolean useMustBeComesAfterDef() {
        return true;
    }

    private Actor actor;
}
